package main

import (
	"anotherdaylearn2/controller"
	"github.com/gin-gonic/gin"
)

type Login struct {
	username string `json:"username"`
	password string `json:"password"`
}

func main() {
	router := SetupRouter()
	router.Run(":8080")
}

//var identityKey = "id"

func SetupRouter() *gin.Engine {
	r := gin.Default()

	v1 := r.Group("nominal")
	{
		v1.GET("/ovop", controller.GetPercentage)
		v1.GET("/list/region/percentage", controller.GetRegion)
		v1.POST("/list/kanca/region", controller.GetKanwil)
		v1.GET("/region/v1", controller.Test)
		v1.POST("/upload", controller.SaveFileHandler)
		v1.POST("/login", controller.UserAuthentication)
		v1.POST("/verify", controller.VerifyToken)
	}
	return r
}
