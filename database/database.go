package database

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
)

func InitDB() *sql.DB {
	db, err := sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/db_inms_branch")
	checkErr(err)

	err = db.Ping()
	checkErr(err)
	//fmt.Printf("Connection successfully")
	return db
}

func checkErr(err error) {
	if err != nil {
		fmt.Print(err.Error())
	}
}
