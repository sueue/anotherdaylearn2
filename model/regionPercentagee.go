package model

type RegionPercentagee struct {
	KodeKanwil string  `json:"kode_kanwil"`
	NamaKanwil string  `json:"nama_kanwil"`
	Persentasi float64 `json:"persentasi"`
}
