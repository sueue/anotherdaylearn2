package model

type RegionPercentage struct {
	KodeKanwil string `json:"kode_kanwil"`
	NamaKanwil string `json:"nama_kanwil"`
	Persentasi string `json:"persentasi"`
}

//type RegionPercentagee struct {
//	KodeKanwil string `json:"kode_kanwil"`
//	NamaKanwil string `json:"nama_kanwil"`
//	Persentasi string `json:"jumlah"`
//}
