package model

type User struct {
	Id           string  `json:"id" gorm:"primary_key"`
	Nama         string  `json:"nama"`
	Email        *string `json:"email"`
	Role         *string `json:"role"`
	Jabatan      *string `json:"jabatan"`
	NomorHp      *string `json:"nomor_hp"`
	KodeProvider *string `json:"kode_provider"`
	KodeKanwil   *string `json:"kode_kanwil"`
	KodeKanca    *string `json:"kode_kanca"`
	CreateAt     *string `json:"create_at"`
	UpdateAt     *string `json:"update_at"`
	MLoginStatus *string `json:"m_login_status"`
	Password     string  `json:"password"`
	Username     string  `json:"username"`
}
