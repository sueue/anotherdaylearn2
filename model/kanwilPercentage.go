package model

type KanwilPercentage struct {
	Persentasi string `json:"persentasi"`
	KodeKanca  string `json:"kode_kanca"`
	KodeKanwil string `json:"kode_kanwil"`
	NamaKanca  string `json:"nama_kanca"`
}
