package model

type Percentage struct {
	Id_remote        int64  `json:"id_remote"`
	Nama_remote      string `json:"nama_remote"`
	Last_update      string `json:"last_update"`
	Last_polled      string `json:"last_polled"`
	Status_rec_date  string `json:"status_rec_date"`
	Status_fail_date string `json:"status_fail_date"`
	Latency          string `json:"latency"`
	Latitude         string `json:"latitude"`
	Longitude        string `json:"longitude"`
	Keterangan       string `json:"keterangan"`
	Pic_pinca        string `json:"pic_pinca"`
	Pic_spo          string `json:"pic_spo"`
	Pet_it           string `json:"pet_it"`
	Pic_uko          string `json:"pic_uko"`
	Pic_kanwil       string `json:"pic_kanwil"`
	AlamatUker       string `json:"alamat_uker"`
	TelpUker         string `json:"telp_uker"`
	KodeTipeUker     string `json:"kode_tipe_uker"`
	TipeUker         string `json:"tipe_uker"`
	NamaKanca        string `json:"nama_kanca"`
	KodeKanca        string `json:"kode_kanca"`
	NamaKanwil       string `json:"nama_kanwil"`
	KodeKanwil       string `json:"kode_kanwil"`
	KodeUker         string `json:"kode_uker"`
	IpLan            string `json:"ip_lan"`
	IpMonitoring     string `json:"ip_monitoring"`
	KodeOpAsli       string `json:"kode_op_asli"`
	StatusAsli       string `json:"status_asli"`
	StatusOnoff      string `json:"status_onoff"`
	StatusAlarm      string `json:"status_alarm"`
	IdAlarmType      string `json:"id_alarm_type"`
	NotesAlarm       string `json:"notes_alarm"`
	Status           string `json:"status"`
	StatusName       string `json:"status_name"`
	KodeSorting      string `json:"kode_sorting"`
	KodeOp           string `json:"kode_op"`
}
