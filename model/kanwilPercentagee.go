package model

type KanwilPercentagee struct {
	Persentasi float64 `json:"persentasi"`
	KodeKanca  string  `json:"kode_kanca"`
	KodeKanwil string  `json:"kode_kanwil"`
	NamaKanca  string  `json:"nama_kanca"`
}
