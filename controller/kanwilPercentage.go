package controller

import (
	"anotherdaylearn2/database"
	"anotherdaylearn2/model"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func GetKanwil(c *gin.Context) {
	var kanwil model.KanwilPercentage
	var kanwill model.KanwilPercentagee
	var kanwill2 model.KanwilPercentagee
	var kanwill3 model.KanwilPercentagee
	var kanwils []model.KanwilPercentage
	var kanwils2 []model.KanwilPercentage
	var kanwilss []model.KanwilPercentagee
	var kanwilss2 []model.KanwilPercentagee
	var kanwilss3 []model.KanwilPercentagee
	var todos string
	kdKanwil := c.PostForm("kd_kanwil")
	var d float64

	todos = kdKanwil

	rows, err := database.InitDB().Query("SELECT DISTINCT kode_kanwil, kode_kanca, nama_kanca FROM v_detail_jarkom WHERE kode_kanwil = ? AND nama_kanca NOT LIKE 'KANWIL%' AND kode_kanwil NOT IN ('T','U','V','Y') ORDER BY nama_kanca ASC", todos)
	if err != nil {
		fmt.Print(err.Error())
	}
	for rows.Next() {
		err = rows.Scan(&kanwil.KodeKanwil, &kanwil.KodeKanca, &kanwil.NamaKanca)
		kanwils = append(kanwils, kanwil)
		if err != nil {
			fmt.Print(err.Error())
		}
	}
	defer rows.Close()
	for i, kdKanwil := range kanwils {
		fmt.Println(i, kdKanwil.KodeKanwil)
		row, err2 := database.InitDB().Query("SELECT COUNT(kode_kanca) AS jumlah, kode_kanca, nama_kanca from v_all_remote WHERE kode_kanca = ? AND status = 3 AND kode_op = 1", kdKanwil.KodeKanca)
		if err2 != nil {
			fmt.Print(err2.Error())
		}
		for row.Next() {
			err2 = row.Scan(&kanwill.Persentasi, &kanwill.KodeKanca, &kanwill.NamaKanca)
			kanwilss = append(kanwilss, kanwill)
			if err2 != nil {
				fmt.Print(err2.Error())
			}
		}
		defer row.Close()

		row2, err3 := database.InitDB().Query("SELECT COUNT(kode_kanca) AS jumlah, kode_kanca, nama_kanca from v_all_remote WHERE kode_kanca = ? AND status = 3 AND kode_op = 2", kdKanwil.KodeKanca)
		if err3 != nil {
			fmt.Print(err3.Error())
		}
		for row2.Next() {
			err3 = row2.Scan(&kanwill2.Persentasi, &kanwill2.KodeKanca, &kanwill2.NamaKanca)
			kanwilss2 = append(kanwilss2, kanwill2)
			if err3 != nil {
				fmt.Print(err3.Error())
			}
		}
		//i++
		defer row2.Close()

		row3, err4 := database.InitDB().Query("SELECT COUNT(kode_kanca) AS jumlah, kode_kanca, nama_kanca from v_all_remote WHERE kode_kanca = ? AND status = 1 AND kode_op = 1 AND kode_tipe_uker NOT IN (6,8,9,10,11,12,13)", kdKanwil.KodeKanca)
		if err4 != nil {
			fmt.Print(err4.Error())
		}
		for row3.Next() {
			err4 = row3.Scan(&kanwill3.Persentasi, &kanwill3.KodeKanca, &kanwill3.NamaKanca)
			kanwilss3 = append(kanwilss3, kanwill3)
			if err4 != nil {
				fmt.Print(err4.Error())
			}
		}
		i++
		defer row3.Close()
		a := kanwill.Persentasi
		b := kanwill2.Persentasi
		c := kanwill3.Persentasi
		d = ((a + b) / (a + b + c) * 100)
		kanwil.NamaKanca = kanwill.NamaKanca
		kanwil.KodeKanwil = kanwil.KodeKanwil
		kanwil.KodeKanca = kanwill.KodeKanca
		kanwil.Persentasi = fmt.Sprintf("%.2f", d)
		kanwils2 = append(kanwils2, kanwil)
	}

	c.JSON(http.StatusOK, gin.H{
		"result": kanwils2,
	})

}
