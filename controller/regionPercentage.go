package controller

import (
	"anotherdaylearn2/database"
	"anotherdaylearn2/model"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func GetRegion(c *gin.Context) {
	var region model.RegionPercentage
	var regionn model.RegionPercentagee
	var regionn2 model.RegionPercentagee
	var regionn3 model.RegionPercentagee
	var regions []model.RegionPercentage
	var regions2 []model.RegionPercentage
	var regionss []model.RegionPercentagee
	var regionss2 []model.RegionPercentagee
	var regionss3 []model.RegionPercentagee
	var d float64

	rows, err := database.InitDB().Query("SELECT DISTINCT `kode_kanwil`, `nama_kanwil` FROM tb_kanwil WHERE `kode_kanwil` NOT IN ('T','U','V','Y')")
	if err != nil {
		fmt.Println(err.Error())
	}

	for rows.Next() {
		err = rows.Scan(&region.KodeKanwil, &region.NamaKanwil)
		regions = append(regions, region)
		if err != nil {
			fmt.Print(err.Error())
		}
	}
	defer rows.Close()
	for i, regional := range regions {
		row, err2 := database.InitDB().Query("SELECT COUNT(kode_kanwil) AS jumlah, kode_kanwil, nama_kanwil from v_all_remote WHERE kode_kanwil = ? AND status = 3 AND kode_op = 1", regional.KodeKanwil)
		if err2 != nil {
			fmt.Print(err2.Error())
		}
		for row.Next() {
			err2 = row.Scan(&regionn.Persentasi, &regionn.KodeKanwil, &regionn.NamaKanwil)
			regionss = append(regionss, regionn)
			if err2 != nil {
				fmt.Print(err2.Error())
			}
		}
		defer row.Close()

		row2, err3 := database.InitDB().Query("SELECT COUNT(kode_kanwil) AS jumlah, kode_kanwil, nama_kanwil from v_all_remote WHERE kode_kanwil = ? AND status = 3 AND kode_op = 2", regional.KodeKanwil)
		if err3 != nil {
			fmt.Print(err3.Error())
		}
		for row2.Next() {
			err3 = row2.Scan(&regionn2.Persentasi, &regionn2.KodeKanwil, &regionn2.NamaKanwil)
			regionss2 = append(regionss2, regionn2)
			if err3 != nil {
				fmt.Print(err3.Error())
			}
		}
		i++
		defer row2.Close()

		row3, err4 := database.InitDB().Query("SELECT COUNT(kode_kanwil) AS jumlah, kode_kanwil, nama_kanwil from v_all_remote WHERE kode_kanwil = ? AND status = 1 AND kode_op = 1 AND kode_tipe_uker NOT IN (6,8,9,10,11,12,13)", regional.KodeKanwil)
		if err4 != nil {
			fmt.Print(err4.Error())
		}
		for row3.Next() {
			err4 = row3.Scan(&regionn3.Persentasi, &regionn3.KodeKanwil, &regionn3.NamaKanwil)
			regionss3 = append(regionss3, regionn3)
			if err4 != nil {
				fmt.Print(err4.Error())
			}
		}
		i++
		defer row3.Close()
		a := regionn.Persentasi
		b := regionn2.Persentasi
		c := regionn3.Persentasi
		d = ((a + b) / (a + b + c) * 100)
		region.NamaKanwil = regionn2.NamaKanwil
		region.KodeKanwil = regionn2.KodeKanwil
		region.Persentasi = fmt.Sprintf("%.2f", d)
		regions2 = append(regions2, region)
	}

	c.JSON(http.StatusOK, gin.H{
		"result": regions2,
	})
}

func Test(c *gin.Context) {
	var region model.RegionPercentage
	//var regionn model.RegionPercentagee
	var regions []model.RegionPercentage
	//var regionss []model.RegionPercentage

	myVariable := []string{"A", "J", "E", "F", "L", "M", "I", "Q", "O", "S", "P", "R", "N", "B", "C", "D", "X", "G", "K", "H"}

	for i, s := range myVariable {
		fmt.Println(i, s)
		rows, err := database.InitDB().Query("SELECT COUNT(status) status, kode_kanwil, nama_kanwil from v_all_remote WHERE kode_kanwil =? AND status = 3 AND kode_op = 1", s)
		//regions = append(regions, region)
		//region.Percent = float64(len(regions))
		if err != nil {
			fmt.Println(err.Error())
		}
		for rows.Next() {
			err = rows.Scan(&region.Persentasi, &region.KodeKanwil, &region.NamaKanwil)
			regions = append(regions, region)
			if err != nil {
				fmt.Print(err.Error())
			}
		}
		defer rows.Close()

	}
	c.JSON(http.StatusOK, gin.H{
		"result": regions,
	})
}
