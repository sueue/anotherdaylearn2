package controller

import (
	"anotherdaylearn2/database"
	"bytes"

	//"bytes"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"time"
)

func SaveFileHandler(c *gin.Context) {
	var buffer bytes.Buffer
	form, _ := c.MultipartForm()
	files := form.File["file"]
	idPresurvey := c.PostForm("id_presurvey")

	t := time.Now()
	year := t.Year()
	s2 := strconv.Itoa(year)

	if err := EnsureBaseDir("filesUpload/" + s2 + "/"); err != nil {
		fmt.Println("Directory creation failed with error: " + err.Error())
		os.Exit(1)
	}

	for _, file := range files {
		extension := filepath.Ext(file.Filename)
		newFileName := uuid.New().String() + extension

		fmt.Println("isi : " + idPresurvey)
		var path = "filesUpload/" + s2 + "/" + newFileName
		fmt.Println("isi 2 : " + path)

		if err := c.SaveUploadedFile(file, "filesUpload/"+s2+"/"+newFileName); err != nil {
			log.Fatal(err)
		}
		stmt, err := database.InitDB().Prepare("INSERT INTO tb_presurvey_pointing_path (id_presurvey,path) values (?,?);")

		if err != nil {
			return
		}
		_, err = stmt.Exec(idPresurvey, "filesUpload/"+s2+"/"+newFileName)
		if err != nil {
			return
		}

		buffer.WriteString("filesUpload/" + s2 + "/" + newFileName)
		buffer.WriteString(" ")
		defer stmt.Close()
		//flowername := buffer.String()
		c.JSON(http.StatusOK, gin.H{
			"message": "Your file has been successfully uploaded.",
		})

	}

}

func EnsureBaseDir(fpath string) error {
	baseDir := path.Dir(fpath)
	info, err := os.Stat(baseDir)
	if err == nil && info.IsDir() {
		return nil
	}
	return os.MkdirAll(baseDir, 0755)
}
