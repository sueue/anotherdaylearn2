package controller

import (
	jwt3 "anotherdaylearn2/auth"
	"anotherdaylearn2/database"
	"anotherdaylearn2/model"
	"fmt"
	jwt "github.com/appleboy/gin-jwt/v2"
	jwt2 "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"log"
	"net/http"
	"time"
)

func UserAuthentication(c *gin.Context) {
	var (
		user        model.User
		result      gin.H
		identityKey = "data"
	)
	username := c.PostForm("username")
	password := c.PostForm("password")
	_, err2 := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	if err2 != nil {
		return
	}
	err := database.InitDB().QueryRow("select id, nama, email, role, jabatan, nomor_hp, kode_provider, kode_kanwil, kode_kanca, create_at, update_at, m_login_status, password, username from tb_user where username = ?", username).Scan(&user.Id, &user.Nama, &user.Email, &user.Role, &user.Jabatan, &user.NomorHp, &user.KodeProvider, &user.KodeKanwil, &user.KodeKanca, &user.CreateAt, &user.UpdateAt, &user.MLoginStatus, &user.Password, &user.Username)
	if err != nil {
		fmt.Println(err, "here")
		result = gin.H{
			"status":  http.StatusUnauthorized,
			"message": "No username found!, Please check for spelling error.",
			"data":    user,
		}
	} else {
		fmt.Println("come here?")
		authMiddleware, err := jwt3.New(&jwt3.GinJWTMiddleware{
			Realm:       "nominal",
			Key:         []byte("nominalapi"),
			Timeout:     time.Hour,
			MaxRefresh:  time.Hour,
			IdentityKey: user.Username,
			IdUser:      user.Id,
			PayloadFunc: func(data interface{}) jwt3.MapClaims {
				fmt.Println("come here payloadfunc")
				if v, ok := data.(*model.User); ok {
					fmt.Println("come 2")
					return jwt3.MapClaims{
						identityKey: v,
					}
				} else {
					fmt.Println("come 3")
					//c.JSON(http.StatusOK, gin.H{"data":user})
					return jwt3.MapClaims{
						identityKey: user,
					}
				}
			},
			IdentityHandler: func(c *gin.Context) interface{} {
				fmt.Println("come here identity handler")
				claims := jwt.ExtractClaims(c)
				return &model.User{
					Username: claims[identityKey].(string),
				}
			},
			Authenticator: func(c *gin.Context) (interface{}, error) {
				fmt.Println("come here authenticator")
				err2 = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
				if err2 != nil {
					fmt.Println(err2.Error())
					result = gin.H{
						"status":  http.StatusUnauthorized,
						"message": "Password not match!, Please check for spelling error",
						"data":    user,
					}
					return nil, jwt.ErrFailedAuthentication
				} else {
					result = gin.H{
						"status":  http.StatusOK,
						"message": "Login success!",
						"data":    user,
					}
				}
				return fmt.Println("Success")
			},
			Authorizator: func(data interface{}, c *gin.Context) bool {
				fmt.Println("isi username user : ", user.Username)
				fmt.Println("isi username post : ", username)
				fmt.Println("come here authorizator")
				if user.Username == username {
					fmt.Println("here true")
					return true
				}
				fmt.Println("here false")
				return false
			},
			Unauthorized: func(c *gin.Context, code int, message string) {
				fmt.Println("come here unauth")
				c.JSON(code, gin.H{
					"code":    code,
					"message": message,
					"token":   "",
				})
			},
			TokenLookup:   "header: Authorization, query: token, cookie: jwt",
			TokenHeadName: "Bearer",
			TimeFunc:      time.Now,
		})
		if err != nil {
			log.Fatal("JWT Error:" + err.Error())
		}

		errInit := authMiddleware.MiddlewareInit()

		if errInit != nil {
			log.Fatal("authMiddleware.MiddlewareInit() Error:" + errInit.Error())
		}

		authMiddleware.LoginHandler(c)
		//
		//claims := jwt.ExtractClaims(c)
		//c.JSON(http.StatusOK, gin.H{
		//	"isi":claims["id"],
		//})

	}
}

func VerifyToken(c *gin.Context) {

	reqToken := c.GetHeader("Authorization")
	fmt.Println(reqToken)
	token, err := jwt2.Parse(reqToken, func(token *jwt2.Token) (i interface{}, err error) {
		return []byte("nominalapi"), nil
	})
	if token.Valid {
		fmt.Println("valid token", err)
		//return fmt.Println("here")
	} else {
		fmt.Println("invalid token", err)
		return
	}
	return
}
