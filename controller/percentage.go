package controller

import (
	"anotherdaylearn2/database"
	"anotherdaylearn2/model"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func GetPercentage(c *gin.Context) {
	var percent model.Percentage
	var percent2 model.Percentage
	var percents []model.Percentage
	var percents2 []model.Percentage

	rows, err := database.InitDB().Query("select * FROM v_all_remote where `status` = 3 AND `kode_op` IN (1,2) AND `kode_kanwil` NOT IN ('T','U','V','Y')")
	//percents = append(percents, percent)
	if err != nil {
		fmt.Print(err.Error())
	}
	for rows.Next() {
		//err = rows.Scan(&percents.)
		percents = append(percents, percent)
		if err != nil {
			fmt.Print(err.Error())
		}
	}
	defer rows.Close()

	rows2, err2 := database.InitDB().Query("SELECT * FROM v_all_remote WHERE `status` = 1 AND kode_op = 1 AND kode_kanwil NOT IN ('T','U','V','Y') AND kode_tipe_uker NOT IN (6,8,9,10,11,12,13)")
	//percents = append(percents, percent)
	if err2 != nil {
		fmt.Print(err2.Error())
	}
	for rows2.Next() {
		//err = rows.Scan(&percents.)
		percents2 = append(percents2, percent2)
		if err2 != nil {
			fmt.Print(err2.Error())
		}
	}
	defer rows2.Close()

	a := float64(len(percents))
	b := float64(len(percents2))
	var d = float64(a/(a+b)) * 100
	c.JSON(http.StatusOK, gin.H{
		"overall_percentage": fmt.Sprintf("%.2f", d),
		"percent":            len(percents),
		"percent2":           len(percents2),
	})
}
